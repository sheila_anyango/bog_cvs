﻿using System;
using System.Collections.Generic;
using System.Text;
using Spire.Doc;
using System.Text.RegularExpressions;
using System.IO;
using Accord.Math;
using System.Linq;

namespace CVBagOfWords
{
    public class BOGModel
    {
        private static List<string> cvtext = new List<string>();
        private static string text = GetTextData(@"C:/Users/ben/Documents/attachments/CV1.docx");
        private static string text2 = GetTextData(@"C:/Users/ben/Documents/attachments/CV9.doc");
        private static string desc_path2 = "C:/Users/ben/Documents/attachments/" +
                "job descriptions/Web Developer.txt";
        private static string cv_path = "C:/Users/ben/Documents/attachments";

        private static string job_description = GetJobText(desc_path2);

        //Create a word dictionary of words found in the job description
        public static Dictionary<string, int> JobWordfrequency = new Dictionary<string, int>();
        public static Dictionary<string, int> CVWordfrequency = new Dictionary<string, int>();

        public static void PrintText()
        {
            Console.WriteLine(text2);
        }
        public static void CleanText()
        {
            char[] string_char = { ' ', '.', ',', ';', ':', '?', '(', ')', '\n','\r'};
            
            string[] words = job_description.Split(string_char);
            int minlength = 2; //minimum number of characters in a word to count
            foreach(string word in words)
            {
                string lowerword = word.Trim().ToLower();
                if (lowerword.Length > minlength)
                {
                    if (!JobWordfrequency.ContainsKey(lowerword))
                    {
                        JobWordfrequency.Add(lowerword, 1);//add a new word in the dictionary
                    }
                    else
                    {
                        JobWordfrequency[lowerword] += 1; //update the occurence off each word in the dictionary
                    }

                }
                

            }

            //print the total word count in the dictionary
            Console.WriteLine("Total word count: {0}", JobWordfrequency.Count);
            //print the ocurrence of each word
            foreach(var word in JobWordfrequency)
            {
                //Console.WriteLine("Total occurence of {0} : {1}", word.Key,  word.Value);
            }

        }

        public static void CVDictionary()
        {
            string cvtext = StopWords.RemoveStopWords(text2);
            string[] words = cvtext.Split(' ');
            int minlength = 2;
            foreach (string word in words)
            {
                string lowerword = word.ToLower();
                if (lowerword.Length > minlength)
                {
                    if (!CVWordfrequency.ContainsKey(lowerword))
                    {
                        CVWordfrequency.Add(lowerword, 1);//add a new word in the dictionary
                    }
                    else
                    {
                        CVWordfrequency[lowerword] += 1; //update the occurence off each word in the dictionary
                    }

                }

            }

            //print the total word count in the dictionary
            Console.WriteLine("Total word count: {0}", CVWordfrequency.Count);
            //print the ocurrence of each word
            foreach (var word in CVWordfrequency)
            {
                //Console.WriteLine("Total occurence of {0} : {1}", word.Key, word.Value);
            }


        }

        public static void KmeansVectors()
        {
            double sum = 0;
            double[] inputvector = new double[JobWordfrequency.Count];
            foreach (KeyValuePair<string, int> pair in CVWordfrequency)
            {
                if (JobWordfrequency.ContainsKey(pair.Key))
                {
                    int index = JobWordfrequency.Keys.ToList().IndexOf(pair.Key);
                    inputvector[index] = pair.Value;
                    //sum = inputvector[index];
                }
            }

            double nums = inputvector.Sum(); 
            

            Console.WriteLine("Input vector");
            Console.WriteLine(nums);

            //CVWordfrequency.Clear();
        }


        public static string GetTextData(string path)
        {
            Document document1 = new Document();
            //document1.LoadFromFile(@"C:/Users/ben/Documents/attachments/CV1.docx");
            document1.LoadFromFile(path);

            //Initialzie StringBuilder Instance
            StringBuilder sb = new StringBuilder();

            //Extract Text from Word and Save to StringBuilder Instance
            foreach (Spire.Doc.Section section in document1.Sections)
            {
                foreach (Spire.Doc.Documents.Paragraph paragraph in section.Paragraphs)

                {

                    sb.AppendLine(paragraph.Text);
                }
            }
            return sb.ToString();

        }

        public static string GetJobText(string path)
        {
            string text = System.IO.File.ReadAllText(path);

            return text.ToString();
        }
    }

    
}

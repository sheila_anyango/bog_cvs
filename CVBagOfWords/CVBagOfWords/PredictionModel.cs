﻿namespace CVBagOfWords
{
    static class PredictionModel
    {
        internal static readonly BOGData cvbog = new BOGData
        {
            SystemDeveloper= 31,
            Developer = 11,
            DataAnalyst = 16,
            FrontEndDeveloper = 9,
            JuniorJavaDeveloper = 24,
            JuniorSystemDeveloper= 18,
            TechnicalProjectManager = 26,
            UXUIDesigner = 8,
            WebDeveloper = 28

        };
    }
}